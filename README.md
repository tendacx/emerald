# Emerald

Emerald is a multi-currency distributed ledger providing a clearing and settlement mechanism.

   - allow partner banks to explore the opportunities distributed ledger technologies can provide to domestic and international clearing and settlement. 
   - built to meet the requirements stated in the draft SEPA Instant Credit Transfer (ICT) rule book
   - built on Ethereum with a module architecture to allow migration to other distributed ledger technologies (eg. Corda, BigChainDB)
   - provides a credible alternative to the incumbent clearing and settlement market infrastructure (ie SWIFT)
   - allow partner banks to pilot cross-border mutli-currency clearing and settlement

### Build Status

| Branch | Status | Coverage | Emerald Test |
| ------ | ------ | -------- | -------- | 
| master | [![build status](https://gitlab.com/emerald-platform/emerald/badges/master/build.svg)](https://gitlab.com/emerald-platform/emerald/commits/master) | [![coverage report](https://gitlab.com/emerald-platform/emerald/badges/master/coverage.svg)](https://gitlab.com/emerald-platform/emerald/commits/master) | ![Emerald badge](https://img.shields.io/badge/emerald-hecto-green.svg) |

### Emerald Test
[Emerald Test](https://gitlab.com/emerald-platform/emerald/wikis/Emerald-Test) aims to define a common performance test for distributed ledger technologies seeking to prove their latency, performance and scalability. This common performance test allows comparison against a number of existing performance benchmarks.

### Getting Started
The Getting started guide will help build an understanding of all the pre-requisites that are required to build and deploy Emerald, it also provides detailed information on the clearing and settlement model.
[Getting Started](https://gitlab.com/emerald-platform/emerald/wikis/get-started)

### Issues
If you have any issues, please raise them as an issue against this repo

### Networks

We will release details of the test net later.

### Contributions
   - All contributions are accepted under the Apache 2.0 license, copyright will be acknowledged in the license
   - Developers should follow the standard [gitlab feature branch workflow](http://docs.gitlab.com/ee/workflow/workflow.html)

### Authors
   - [Richard Crook](https://gitlab.com/emerald-platform/emerald/wikis/authors-of-emerald)
   - [Mark Hornsby](https://gitlab.com/emerald-platform/emerald/wikis/authors-of-emerald)
   - [Maria Khan](https://gitlab.com/emerald-platform/emerald/wikis/authors-of-emerald)
   - [Mark Simpson](https://gitlab.com/emerald-platform/emerald/wikis/authors-of-emerald)
   - [Ben Wyeth](https://gitlab.com/emerald-platform/emerald/wikis/authors-of-emerald)



>It is worth noting that the copyright added to the Emerald code protects RBS from any liability for any conduct of mis-use of the code shared, 
this is in no way an act of asserting any form or degree of ownership over the code shared. Emerald is an alpha version and should not be assumed production ready. 

### For help...

GFT helped us with the initial performance testing of Emerald so they are good people to ask for support if needed...

<div style="text-align:center"><img src="static/images/gft.jpg" height="75"/></div>
